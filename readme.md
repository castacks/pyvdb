# pyvdb

A specialized openvdb wrapper.

apt-get dependencies:
- libopenvdb-dev
- libopenexr-dev
- libblosc-dev

- note: doesn't seem to work with gcc 4.x. I use gcc 5.x and it's fine.

## License

BSD, see LICENSE.
