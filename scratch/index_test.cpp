#include <iostream>

#include "openvdb/openvdb.h"

namespace vdb = openvdb;
namespace vdbm = openvdb::math;

int main(int argc, char *argv[]) {

  vdb::initialize();

  vdb::FloatGrid grid;
  grid.setTransform(vdbm::Transform::createLinearTransform(0.1));

  vdb::FloatGrid::Accessor acc = grid.getAccessor();

  vdb::Coord ijk(10, 20, 30);
  acc.setValue(ijk, 2.0);

  vdb::Vec3d xyz(10, 20, 30);

  vdb::Vec3d wpt = grid.indexToWorld(ijk);
  std::cout << "world pt: " << wpt << "\n";

  vdb::Vec3d wpt2 = grid.indexToWorld(xyz);
  std::cout << "world pt 2: " << wpt2 << "\n";

  vdb::Coord ijk2(1, 2, 3);
  acc.setValue(ijk2, 8.0);

  for (vdb::FloatGrid::ValueOnCIter itr = grid.cbeginValueOn();
       itr;
       ++itr) {
    std::cout << "Grid" << itr.getCoord() << " = " << *itr << std::endl;
  }

  float a = acc.getValue(vdb::Coord(10, 20, 30));
  float b = acc.getValue(vdb::Coord(1, 2, 3));

  std::cout << "acc.getValue(10, 20, 30) = " << a << std::endl;
  std::cout << "acc.getValue(1, 2, 3) = " << b << std::endl;

  return 0;
}
