import numpy as np
import pyvdb

def test1():
    # g = pyvdb.FloatGrid()

    #g = pyvdb.read_grids('scan.5563.hit.02.rot.vdb')[0]
    g = pyvdb.read_grids('tmp.vdb')[0]
    print type(g)

    # pyvdb.write_grids('tmp2.vdb', [g])

    bb = g.eval_active_voxel_bbox()
    print bb
    #b0 = pyvdb.Coord(-100, -100, -100)
    #b1 = pyvdb.Coord(100, 100, 100)
    #b0 = pyvdb.Coord(-20, -20, -20)
    #b1 = pyvdb.Coord(20, 20, 20)
    b0 = pyvdb.Coord(-10, -10, -10)
    b1 = pyvdb.Coord(10, 10, 10)
    #b0 = pyvdb.Coord(-2, -2, -2)
    #b1 = pyvdb.Coord(2, 2, 2)

    #arr = pyvdb.copy_to_array(g, b0, b1)
    arr = g.copy_to_array(b0, b1)
    print arr.shape
    print arr.dtype
    print arr.sum()
    print arr.max()
    #print arr
    #print arr
    #print 'bax'

    g2 = pyvdb.FloatGrid()
    g2.copy_from_array(arr, pyvdb.Coord(0, 0, 0), 0)
    print g2

    arr2 = g2.copy_to_array(pyvdb.Coord(0, 0, 0),
                            pyvdb.Coord(20, 20, 20))
    print arr2.sum()


def test2():
    g = pyvdb.read_grids('tmp.vdb')[0]
    print g.active_voxel_count()
    # actually doesn't seem to do anything?
    g.prune(0.1)
    g.prune_inactive()
    print g.active_voxel_count()

def test3():
    g = pyvdb.read_grids('tmp.vdb')[0]
    print g.active_voxel_count()
    arr = pyvdb.grid_to_xyz_ndarray(g, 4)
    print arr.dtype
    print arr.shape
    print arr['value'].max()

g = pyvdb.FloatGrid()
arr = np.array([[4, 0, 0]], dtype='f4')
#pyvdb.update_hits_from_ndarray(g, arr)
for _ in range(10):
    pyvdb.update_occmap_from_ndarray(g, arr, [0., 0., 0.])
print g
arr2 = pyvdb.grid_to_xyz_ndarray(g, -2.0)
print arr2





