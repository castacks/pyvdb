#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include "openvdb/openvdb.h"
#include "openvdb/tools/Dense.h"
#include "openvdb/tools/Prune.h"

#include "typedefs.h"

namespace pyvdb {

namespace py = pybind11;
namespace vdb = openvdb;
namespace vdbm = openvdb::math;

template <class GridT>
py::array_t<typename GridT::ValueType> copy_to_array(const GridT& grid,
                                                     vdb::Coord min_ix,
                                                     vdb::Coord max_ix) {
  using value_type = typename GridT::ValueType;

  for (size_t i=0; i < 3; ++i) {
    // TODO check if +1 is necessary - are limits inclusive?
    if (max_ix[i] <= min_ix[i]) {
      throw std::runtime_error("max_ix must be larger than min_ix");
    }
  }

  // forgot about the lazy loading stuff.
  // reading everything makes things work -- otherwise stuff is 0
  grid.readNonresidentBuffers();

  vdb::CoordBBox arr_bbox = vdb::CoordBBox(min_ix, max_ix);
  std::vector<ssize_t> shape;

  shape.push_back(max_ix[0] - min_ix[0]);
  shape.push_back(max_ix[1] - min_ix[1]);
  shape.push_back(max_ix[2] - min_ix[2]);

  for (size_t i=0; i < 3; ++i) {
    arr_bbox.max()[i] -= 1;
  }

  py::array_t<value_type, py::array::c_style> arr(shape);

  vdb::tools::Dense<value_type> dense(arr_bbox, arr.mutable_data());
  vdb::tools::copyToDense(grid, dense, false);
  //vdb::tools::copyToDense(*grid, dense);

  return arr;
}


template <class GridT>
void copy_from_array(GridT& grid,
                     py::array_t<typename GridT::ValueType, 3> arr,
                     vdb::Coord origin,
                     typename GridT::ValueType tolerance) {
  using value_type = typename GridT::ValueType;

  vdb::Coord max_ix(origin[0] + arr.shape(0) - 1,
                    origin[1] + arr.shape(1) - 1,
                    origin[2] + arr.shape(2) - 1);
  vdb::CoordBBox arr_bbox = vdb::CoordBBox(origin, max_ix);

  //vdb::tools::Dense<value_type> dense(arr_bbox, arr.data());
  vdb::tools::Dense<value_type> dense(arr_bbox, arr.mutable_data());
  vdb::tools::copyFromDense(dense, grid, tolerance);
}


template<class VecT>
void export_vec(py::module& m, const char* name) {
  using value_type = typename VecT::value_type;
  py::class_<VecT>(m, name, py::buffer_protocol())
      .def(py::init<>())
      .def(py::init<VecT>())
      .def(py::init([](py::buffer buf) {
        py::buffer_info info = buf.request();

        if (info.format != py::format_descriptor<value_type>::format()) {
          throw std::runtime_error("incompatible format");
        }

        if (info.ndim != 1) {
          throw std::runtime_error("incompatible buffer dim");
        }

        if (info.shape[0] != VecT::size) {
          throw std::runtime_error("incompatible size");
        }

        value_type* ptr = reinterpret_cast<value_type*>(info.ptr);

        return new VecT(ptr);
      }))
      .def_buffer([](VecT& v) -> py::buffer_info {
        return py::buffer_info(
            reinterpret_cast<uint8_t*>(v.asPointer()),
            sizeof(typename VecT::value_type),
            py::format_descriptor<typename VecT::value_type>::format(),
            1,
            { v.size },
            { sizeof(typename VecT::value_type) }
        );
      })
      .def("__str__", &VecT::str)
      .def("__setitem__", [](VecT& self, int i, value_type val) { self[i] = val; })
      .def("__getitem__", [](VecT& self, int i) { return self[i]; })
      ;
}

void export_vectors(py::module& m) {
  pyvdb::export_vec<vdb::Vec2i>(m, "Vec2i");
  pyvdb::export_vec<vdb::Vec2I>(m, "Vec2I");
  pyvdb::export_vec<vdb::Vec2s>(m, "Vec2s");
  pyvdb::export_vec<vdb::Vec2d>(m, "Vec2d");

  pyvdb::export_vec<vdb::Vec3i>(m, "Vec3i");
  pyvdb::export_vec<vdb::Vec3I>(m, "Vec3I");
  pyvdb::export_vec<vdb::Vec3s>(m, "Vec3s");
  pyvdb::export_vec<vdb::Vec3d>(m, "Vec3d");
  // TODO more as needed
}

void export_coords(py::module& m) {
  py::class_<vdb::Coord>(m, "Coord", py::buffer_protocol())
      .def(py::init<>())
      .def(py::init<vdb::Vec3i>())
      .def(py::init<vdb::Vec3I>())
      .def(py::init<int32_t, int32_t, int32_t>())
      .def(py::init([](py::buffer buf) {
        py::buffer_info info = buf.request();
        if (info.format != py::format_descriptor<int32_t>::format()) {
          throw std::runtime_error("incompatible format");
        }

        if (info.ndim != 1) {
          throw std::runtime_error("incompatible buffer dim");
        }

        if (info.shape[0] != 3) {
          throw std::runtime_error("incompatible size");
        }

        int32_t* ptr = reinterpret_cast<int32_t*>(info.ptr);
        return new vdb::Coord(ptr[0], ptr[1], ptr[2]);
      }))
      .def_buffer([](vdb::Coord& c) -> py::buffer_info {
        return py::buffer_info(
            reinterpret_cast<uint8_t*>(c.asPointer()),
            sizeof(int32_t),
            py::format_descriptor<int32_t>::format(),
            1,
            { 3 },
            { sizeof(int32_t) }
        );
      })
      .def("__repr__", [](const vdb::Coord& c) {
        std::stringstream ss;
        ss << "Coord(" << c.x() << ", " << c.y() << ", " << c.z() << ")";
        return ss.str();
      })
      .def("x", (int32_t (vdb::Coord::*)() const) &vdb::Coord::x)
      .def("y", (int32_t (vdb::Coord::*)() const) &vdb::Coord::y)
      .def("z", (int32_t (vdb::Coord::*)() const) &vdb::Coord::z)
      .def("set_x", &vdb::Coord::setX)
      .def("set_y", &vdb::Coord::setY)
      .def("set_z", &vdb::Coord::setZ)
      .def("as_vec3d", &vdb::Coord::asVec3d)
      .def("as_vec3i", &vdb::Coord::asVec3i)
      .def("reset", (vdb::Coord& (vdb::Coord::*)(int32_t, int32_t, int32_t)) &vdb::Coord::reset)
  ;
}

void set_voxel_size(vdb::GridBase& grid, double vs) {
  // cell-centered
  const vdbm::Vec3d offset(vs/2., vs/2., vs/2.);
  vdbm::Transform::Ptr tf = vdb::math::Transform::createLinearTransform(vs);
  tf->postTranslate(offset);
  grid.setTransform(tf);
  // can also add other rotations/translations
}

vdbm::Vec3d get_voxel_size(const vdb::GridBase& grid) {
  vdbm::Vec3d v = grid.voxelSize();
  return v;
}

template <class GridT>
typename GridT::ValueType get_value(const typename GridT::Ptr grid,
                                    const vdb::Coord& ijk) {
  typename GridT::Accessor acc(grid->getAccessor());
  typename GridT::ValueType val = acc.getValue(ijk);
  return val;
}

template <class GridT>
void set_value(const typename GridT::Ptr grid,
               const vdb::Coord& ijk,
               typename GridT::ValueType val) {
  typename GridT::Accessor acc(grid->getAccessor());
  acc.setValue(ijk, val);
}

void export_grid_base(py::class_<vdb::GridBase, vdb::GridBase::Ptr>& GridBase) {

  vdb::Vec3d (vdb::GridBase::* i2w_v)(const vdb::Vec3d &) const = &vdb::GridBase::indexToWorld;

  GridBase.def_property("voxel_size", &pyvdb::get_voxel_size, &pyvdb::set_voxel_size)
  .def_property("name", &vdb::GridBase::setName, &vdb::GridBase::getName)
  .def("empty", &vdb::GridBase::empty)
  .def("clear", &vdb::GridBase::clear)
  .def("clip", [](vdb::GridBase& self,
                  const vdb::Coord& minpt,
                  const vdb::Coord& maxpt) {
        vdb::CoordBBox cbbox(minpt, maxpt);
        self.clip(cbbox);
       },
       "clip in index space")
  .def("clip_grid", [](vdb::GridBase& self,
                       const vdb::Vec3d& minpt,
                       const vdb::Vec3d& maxpt) {
        vdb::BBoxd bbox(minpt, maxpt);
        self.clipGrid(bbox);
       },
       "clip in world space")
  .def("active_voxel_count", &vdb::GridBase::activeVoxelCount)
  .def("eval_active_voxel_bbox", [](const vdb::GridBase& self) {
       vdb::CoordBBox cbb = self.evalActiveVoxelBoundingBox();
       vdb::Coord cmin = cbb.min();
       vdb::Coord cmax = cbb.max();
       py::tuple out = py::make_tuple(cmin, cmax);
       return out;
   })
  .def("__str__", [](vdb::GridBase& self) {
       std::stringstream ss;
       self.print(ss);
       std::string out(ss.str());
       return out;
   })
  .def("copy", &vdb::GridBase::deepCopyGrid)
  .def("world_to_index", &vdb::GridBase::worldToIndex)
  .def("index_to_world", i2w_v)
  .def("mem_usage", &vdb::GridBase::memUsage)
  .def("read_nonresident_buffers", &vdb::GridBase::readNonresidentBuffers)
  ;
}

template <class GridT>
void export_grid_t(py::module& m,
                   const char* name,
                   py::class_<vdb::GridBase, vdb::GridBase::Ptr>& base_class) {
  using value_type = typename GridT::ValueType;
  py::class_<GridT, typename GridT::Ptr>(m, name, base_class)
      .def(py::init<>())
      .def(py::init<value_type>(), py::arg("background_value"))
      .def(py::init<GridT>())
      .def("get_value", &get_value<GridT>,
           py::arg("ijk"))
      .def("set_value", &set_value<GridT>,
           py::arg("ijk"),
           py::arg("value"))
      .def("copy_to_array", &copy_to_array<GridT>,
           py::arg("min_ix"),
           py::arg("max_ix"))
      .def("copy_from_array",
           &copy_from_array<GridT>,
           py::arg("self"),
           py::arg("origin"),
           py::arg("tolerance"))
      .def("prune", [](GridT& grid, value_type tolerance) {
        vdb::tools::prune(grid.tree(), tolerance);
      })
      .def("prune_inactive", [](GridT& grid) {
        vdb::tools::pruneInactive(grid.tree());
      })
      ;
}


template <class GridT>
void export_grid_t_no_numpy(py::module& m,
                            const char* name,
                            py::class_<vdb::GridBase, vdb::GridBase::Ptr>& base_class) {
  using value_type = typename GridT::ValueType;
  py::class_<GridT, typename GridT::Ptr>(m, name, base_class)
      .def(py::init<>())
      .def(py::init<value_type>(), py::arg("background_value"))
      .def(py::init<GridT>())
      .def("get_value", &get_value<GridT>,
           py::arg("ijk"))
      .def("set_value", &set_value<GridT>,
           py::arg("ijk"),
           py::arg("value"))
      .def("prune", [](GridT& grid, value_type tolerance) {
        vdb::tools::prune(grid.tree(), tolerance);
      })
      .def("prune_inactive", [](GridT& grid) {
        vdb::tools::pruneInactive(grid.tree());
      })
      ;
}


void export_grids(py::module& m) {
  // TODO maybe just convert to GridBase
  pyvdb::export_coords(m);
  pyvdb::export_vectors(m);

  py::class_<vdb::GridBase, vdb::GridBase::Ptr> GridBase(m, "_GridBase");
  pyvdb::export_grid_base(GridBase);

  pyvdb::export_grid_t<vdb::FloatGrid>(m, "FloatGrid", GridBase);
  pyvdb::export_grid_t<vdb::DoubleGrid>(m, "DoubleGrid", GridBase);
  pyvdb::export_grid_t<vdb::Int32Grid>(m, "Int32Grid", GridBase);
  pyvdb::export_grid_t<vdb::Int64Grid>(m, "Int64Grid", GridBase);
  pyvdb::export_grid_t<vdb::BoolGrid>(m, "BoolGrid", GridBase);
  //pyvdb::export_grid_t<vdb::Vec3IGrid>(m, "Vec3IGrid", GridBase);
  //pyvdb::export_grid_t<vdb::Vec3SGrid>(m, "Vec3SGrid", GridBase);
  //pyvdb::export_grid_t<vdb::Vec3DGrid>(m, "Vec3DGrid", GridBase);
  pyvdb::export_grid_t_no_numpy<vdb::StringGrid>(m, "StringGrid", GridBase);
}

} // namespace
