#include <iostream>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include "openvdb/openvdb.h"
#include "openvdb/io/Stream.h"

#include "typedefs.h"

namespace pyvdb {

namespace py = pybind11;
namespace vdb = openvdb;
namespace vdbm = openvdb::math;

void write_grids(const std::string& fname, py::list grids) {
  vdb::io::File file(fname);
  vdb::GridPtrVec gridvec;
  for (auto obj : grids) {
    vdb::GridBase::Ptr grid = obj.cast<vdb::GridBase::Ptr>();
    gridvec.push_back(grid);
  }
  file.write(gridvec);
  file.close();
}

py::list read_grids(const std::string& fname) {
  vdb::io::File file(fname);
  file.open();
  vdb::GridPtrVecPtr gridvec = file.getGrids();
  //vdb::MetaMap::Ptr metadata = file.getMetadata();
  file.close();
  py::list grid_list;
  for (vdb::GridPtrVec::const_iterator it = gridvec->begin();
       it != gridvec->end();
       ++it) {
    vdb::GridBase::Ptr grid = *it;
    py::object grid_obj = py::cast(grid);
    //MyClass *cls = obj.cast<MyClass *>();
    grid_list.append(grid_obj);
  }
  return grid_list;
}

py::bytes write_grids_to_string(py::list grids) {
  vdb::GridPtrVec gridvec;
  for (auto obj : grids) {
    vdb::GridBase::Ptr grid = obj.cast<vdb::GridBase::Ptr>();
    gridvec.push_back(grid);
  }

  std::ostringstream ostr(std::ios::binary);
  vdb::io::Stream vdb_ostr(ostr);
  vdb_ostr.write(gridvec);
  //vdb_ostr.setCompression(vdb::io::COMPRESS_BLOSC | vdb::io::COMPRESS_ACTIVE_MASK);
  vdb_ostr.setCompression(vdb::io::COMPRESS_ZIP | vdb::io::COMPRESS_ACTIVE_MASK);

  std::string tmp(ostr.str());
  return py::bytes(tmp);
}

py::list read_grids_from_string(std::string& buf) {

  std::istringstream istr(buf, std::ios_base::binary);
  vdb::io::Stream vdb_istr(istr);

  vdb_istr.setCompression(vdb::io::COMPRESS_BLOSC | vdb::io::COMPRESS_ACTIVE_MASK);

  vdb::GridPtrVecPtr gridvec = vdb_istr.getGrids();
  py::list grid_list;
  for (vdb::GridPtrVec::const_iterator it = gridvec->begin();
       it != gridvec->end();
       ++it) {
    vdb::GridBase::Ptr grid = *it;
    py::object grid_obj = py::cast(grid);
    grid_list.append(grid_obj);
  }
  return grid_list;
}

void export_io(py::module& m) {
  m.def("write_grids", &write_grids);
  m.def("read_grids", &read_grids);
  m.def("read_grids_from_string", &read_grids_from_string);
  m.def("write_grids_to_string", &write_grids_to_string);
}

}
