#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include "openvdb/openvdb.h"

#include "typedefs.h"

namespace py = pybind11;
namespace vdb = openvdb;
namespace vdbm = openvdb::math;

namespace pyvdb {

template <class T>
struct ValueToNumpyFormat { };

template <> struct ValueToNumpyFormat<float> {
  static constexpr const char * format() { return "f4"; }
};

template <> struct ValueToNumpyFormat<int> {
  static constexpr const char * format() { return "i4"; }
};

template <> struct ValueToNumpyFormat<double> {
  static constexpr const char * format() { return "f8"; }
};


template <class GridT>
py::array grid_to_xyz_ndarray(typename GridT::Ptr grid,
                              typename GridT::ValueType thresh) {

  using value_type = typename GridT::ValueType;
  using tree_type = typename GridT::TreeType;
  using itr_type = typename GridT::ValueOnCIter;

  //typename GridT::Accessor acc = grid->getAccessor();
  const vdb::math::Transform& grid_tf(grid->transform());

  // just a vector of bytes
  std::vector<uint8_t> pts;
  pts.reserve(grid->activeVoxelCount()*16);

  ssize_t num_pts = 0;
  for (itr_type itr = grid->cbeginValueOn(); itr.test(); ++itr) {
    if (!itr.isVoxelValue()) {
      continue;
    }
    value_type val = itr.getValue();
    if (val < thresh) {
      continue;
    }
    vdb::Coord ijk = itr.getCoord();
    vdb::Vec3d p = grid_tf.indexToWorld(ijk);

    float x = static_cast<float>(p.x());
    float y = static_cast<float>(p.y());
    float z = static_cast<float>(p.z());
    pts.insert(pts.end(), reinterpret_cast<uint8_t*>(&x), reinterpret_cast<uint8_t*>(&x)+sizeof(x));
    pts.insert(pts.end(), reinterpret_cast<uint8_t*>(&y), reinterpret_cast<uint8_t*>(&y)+sizeof(y));
    pts.insert(pts.end(), reinterpret_cast<uint8_t*>(&z), reinterpret_cast<uint8_t*>(&z)+sizeof(z));
    pts.insert(pts.end(), reinterpret_cast<uint8_t*>(&val), reinterpret_cast<uint8_t*>(&val)+sizeof(val));
    num_pts++;
  }

  py::list names, formats, offsets;
  names.append("x");
  names.append("y");
  names.append("z");
  names.append("value");
  formats.append("f4");
  formats.append("f4");
  formats.append("f4");
  formats.append(ValueToNumpyFormat<value_type>::format());
  offsets.append(0);
  offsets.append(4);
  offsets.append(8);
  offsets.append(12);
  ssize_t itemsize = sizeof(float)*3 + sizeof(value_type);

  py::dtype dt(names, formats, offsets, itemsize);

  // TODO can we move the data instead of copying?
  py::array out(dt, num_pts);
  uint8_t* out_ptr = reinterpret_cast<uint8_t*>(out.mutable_data());
  std::copy(pts.begin(), pts.end(), out_ptr);
  return out;
}

void export_misc(py::module& m) {
  m.def("grid_to_xyz_ndarray",
        &grid_to_xyz_ndarray<vdb::FloatGrid>,
        py::arg("grid"),
        py::arg("threshold")=0);
}

} // namespace
