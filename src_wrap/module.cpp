#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include "openvdb/openvdb.h"

#include "typedefs.h"

namespace py = pybind11;

namespace pyvdb {

void export_voxelize(py::module& m);
void export_grids(py::module& m);
void export_misc(py::module& m);
void export_io(py::module& m);

} // namespace

PYBIND11_MODULE(libpyvdb, m) {
  m.doc() = "libpyvdb";
  openvdb::initialize();

  pyvdb::export_voxelize(m);
  pyvdb::export_grids(m);
  pyvdb::export_misc(m);
  pyvdb::export_io(m);

}
