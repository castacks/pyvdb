#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/eigen.h>

#include "openvdb/openvdb.h"
#include "openvdb/tools/Dense.h"
#include "openvdb/tools/Prune.h"
#include "openvdb/math/Ray.h"
#include "openvdb/math/DDA.h"

#include "typedefs.h"

namespace py = pybind11;
namespace vdb = openvdb;
namespace vdbm = openvdb::math;

namespace pyvdb {

template <class GridT>
void update_hits_from_ndarray(typename GridT::Ptr grid,
                              const ndarray2f& xyz,
                              typename GridT::ValueType incr_val) {

  if (xyz.shape(1) != 3) {
    throw std::runtime_error("xyz array must be Nx3");
  }

  using value_type = typename GridT::ValueType;
  typename GridT::Accessor acc = grid->getAccessor();
  const vdb::math::Transform& grid_tf(grid->transform());

  auto buf = xyz.unchecked();
  for (ssize_t i=0; i < xyz.shape(0); ++i) {
    vdb::Vec3d p(buf(i, 0), buf(i, 1), buf(i, 2));
    //TODO do we need cell centered if tf is set correctly?
    vdb::Coord ijk = grid_tf.worldToIndexCellCentered(p);
    int cnt = acc.getValue(ijk);
    acc.setValue(ijk, cnt+incr_val);
  }
}


void update_hits_from_ndarray_bool(vdb::BoolGrid::Ptr grid,
                                   const ndarray2f& xyz) {
  if (xyz.shape(1) != 3) {
    throw std::runtime_error("xyz array must be Nx3");
  }

  vdb::BoolGrid::Accessor acc = grid->getAccessor();
  const vdb::math::Transform& grid_tf(grid->transform());

  auto buf = xyz.unchecked();
  for (ssize_t i=0; i < xyz.shape(0); ++i) {
    vdb::Vec3d p(buf(i, 0), buf(i, 1), buf(i, 2));
    //TODO do we need cell centered if tf is set correctly?
    vdb::Coord ijk = grid_tf.worldToIndexCellCentered(p);
    acc.setValue(ijk, true);
  }
}


void update_occmap_from_ndarray(vdb::FloatGrid::Ptr grid_logocc,
                                const ndarray2f& xyz,
                                const ndarray1f& origin,
                                float l_free,
                                float l_occ,
                                float l_min,
                                float l_max,
                                int hit_thickness) {
  // octomap
  // typical values for l_occ,l_free = .85, -.4
  // also clamping at -2, 3.5
  if (xyz.shape(1) != 3) {
    throw std::runtime_error("xyz array must be Nx3");
  }

  if (origin.size() != 3) {
    throw std::runtime_error("origin array must have size 3");
  }

  vdb::FloatGrid::Accessor acc = grid_logocc->getAccessor();

  vdb::Vec3d origin2(*origin.data(0), *origin.data(1), *origin.data(2));
  vdb::Vec3d origin_ijk = grid_logocc->worldToIndex(origin2);

  auto xyz_buf = xyz.unchecked();

  for (int i=0; i < xyz.shape(0); ++i) {

    vdb::Vec3d p_ijk = grid_logocc->worldToIndex(vdb::Vec3d(xyz_buf(i, 0),
                                                            xyz_buf(i, 1),
                                                            xyz_buf(i, 2)));

    vdb::Vec3d dir(p_ijk-origin_ijk);
    double range = dir.length();
    dir.normalize();
    vdbm::Ray<double> ray(origin_ijk, dir);
    // ray, start_time, max_time
    vdbm::DDA<vdbm::Ray<double>, 0> dda(ray, 0., range);
    // want to stop before dda.time == dda.maxTime
    do {
      vdb::Coord ijk(dda.voxel());
      float ll = acc.getValue(ijk);
      acc.setValue(ijk, std::max(l_min, ll+l_free));
      dda.step();
    } while (dda.time() < dda.maxTime());
    // post condition, at hit voxel
    for (int i=0; i < hit_thickness; ++i) {
      vdb::Coord ijk(dda.voxel());
      float ll = acc.getValue(ijk);
      acc.setValue(ijk, std::min(l_max, ll+l_occ));
      dda.step();
    }
  }
}


struct ix2d_hash : public std::unary_function<std::tuple<int, int>, std::size_t> {
  std::size_t operator()(const std::tuple<int, int>& k) const {
    int h1 = 73856093*std::get<0>(k);
    int h2 = 19349669*std::get<1>(k);
    return h1^h2;
  }
};

struct {
  bool operator()(std::tuple<int, float>& a, std::tuple<int, float>& b) {
    return std::get<0>(a) < std::get<0>(b);
  }
} voxval_cmp;

py::array heightmap_from_grid_logocc(vdb::FloatGrid::Ptr grid,
                                     float thresh,
                                     float thresh2,
                                     int minvox) {
  using itr_type = vdb::FloatGrid::ValueOnCIter;

  vdb::FloatGrid::Accessor acc = grid->getAccessor();

  const vdb::math::Transform& grid_tf(grid->transform());

  vdb::CoordBBox cbbox = grid->evalActiveVoxelBoundingBox();
  int min_i = cbbox.min()[0];
  int min_j = cbbox.min()[1];
  int min_k = cbbox.min()[2];

  int max_i = cbbox.max()[0];
  int max_j = cbbox.max()[1];
  int max_k = cbbox.max()[2];

  int dim_i = max_i - min_i;
  int dim_j = max_j - min_j;
  int dim_k = max_k - min_k;

  //py::print("dim_i", dim_i, "dim_j", dim_j, "dim_k", dim_k);

  using ix2d = std::tuple<int, int>;
  using vox_val = std::tuple<int, float>;

  std::unordered_map<const ix2d, std::vector<vox_val>, ix2d_hash> ij_vals;

  for (itr_type itr = grid->cbeginValueOn(); itr.test(); ++itr) {
    if (!itr.isVoxelValue()) {
      continue;
    }
    float val = itr.getValue();
    if (val < thresh) {
      continue;
    }
    vdb::Coord ijk = itr.getCoord();

    // key is ij
    std::tuple<int, int> key(ijk[0], ijk[1]);
    // value is k, voxel logocc val
    std::tuple<int, float> kval(ijk[2], val);
    ij_vals[key].push_back(kval);
  }

  Eigen::VectorXf column(dim_k+1);
  Eigen::VectorXf column_lt(dim_k+1);
  Eigen::VectorXf column_gt(dim_k+1);

  std::vector<uint8_t> out_bytes;
  ssize_t num_pts = 0;
  for (auto& ij_kval : ij_vals) {
    const ix2d& ij(ij_kval.first);
    const std::vector<vox_val>& kvals(ij_kval.second);

    if (kvals.size() < minvox) {
      continue;
    }

    //std::sort(kvals.begin(), kvals.end(), voxval_cmp);
    column.setZero();
    column_lt.setZero();
    column_gt.setZero();

    float maxval = thresh;
    // insert all voxel values into column
    for (int i=0; i < kvals.size(); ++i) {
      const vox_val& vv(kvals[i]);
      int k(std::get<0>(vv));
      float val(std::get<1>(vv));
      int offset_k = k-min_k;
      column[offset_k] = val;
      maxval = std::max(maxval, val);
    }

    if (maxval < thresh2) {
      continue;
    }

    // compute for each k, sum(val[i]) st. i < k
    column_lt[0] = column[0];
    for (int i=1; i < column.size(); ++i) {
      column_lt[i] = column_lt[i-1]+column[i];
    }
    // compute for each k, sum(val[i]) st. i > k
    column_gt[column.size()-1] = column[column.size()-1];
    for (int i=column.size()-2; i >= 0; --i) {
      column_gt[i] = column_gt[i+1]+column[i];
    }

    //for (int i=0; i < column.size(); ++i) {
    //  py::print(i, column_gt[i]-column_lt[i]);
    //}


    int mindiff = column_gt[0] - column_lt[0];
    int argminval = 0;
    if (column.size() > 1) {
      for (int i=1; i < column.size(); ++i) {
        float diff = column_gt[i] - column_lt[i];
        if (diff < mindiff) {
          mindiff = diff;
          argminval = i;
        }
      }
    }
    argminval += min_k;

    //py::print("mindiff:", mindiff);

    vdb::Coord ijk(std::get<0>(ij), std::get<1>(ij), argminval);
    vdb::Vec3d p = grid_tf.indexToWorld(ijk);

    //py::print("argminval:", argminval);
    float x = static_cast<float>(p.x());
    float y = static_cast<float>(p.y());
    float z = static_cast<float>(p.z());
    out_bytes.insert(out_bytes.end(), reinterpret_cast<uint8_t*>(&x), reinterpret_cast<uint8_t*>(&x)+sizeof(x));
    out_bytes.insert(out_bytes.end(), reinterpret_cast<uint8_t*>(&y), reinterpret_cast<uint8_t*>(&y)+sizeof(y));
    out_bytes.insert(out_bytes.end(), reinterpret_cast<uint8_t*>(&z), reinterpret_cast<uint8_t*>(&z)+sizeof(z));
    num_pts++;
  }

  if (num_pts == 0) {
    py::array out;
    return out;
  }

  //py::array_t<float, 2> out({num_pts, ssize_t(3)});
  py::array_t<float> out({num_pts, ssize_t(3)});
  uint8_t* out_ptr = reinterpret_cast<uint8_t*>(out.mutable_data());
  std::copy(out_bytes.begin(), out_bytes.end(), out_ptr);
  return out;
}


void export_voxelize(py::module& m) {
  m.def("update_hits_from_ndarray",
        &update_hits_from_ndarray<vdb::FloatGrid>,
        py::arg("grid"),
        py::arg("arr"),
        py::arg("incr_val")=1.0f,
        "voxelize hits from Nx3 float ndarray, adding incr_val");
  m.def("update_hits_from_ndarray",
        &update_hits_from_ndarray<vdb::Int32Grid>,
        py::arg("grid"),
        py::arg("arr"),
        py::arg("incr_val")=1,
        "voxelize hits from Nx3 float ndarray, adding incr_val");
  m.def("update_hits_from_ndarray",
        &update_hits_from_ndarray_bool,
        py::arg("grid"),
        py::arg("arr"),
        "voxelize boolean hits from Nx3 float ndarray");
  m.def("update_occmap_from_ndarray",
        &update_occmap_from_ndarray,
        py::arg("grid_logocc"),
        py::arg("xyz"),
        py::arg("origin"),
        py::arg("l_free")=-0.4,
        py::arg("l_occ")=0.85,
        py::arg("l_min")=-2.0,
        py::arg("l_max")=3.5,
        py::arg("hit_thickness")=1);
  m.def("heightmap_from_grid_logocc",
        &heightmap_from_grid_logocc);

}

} // namespace
